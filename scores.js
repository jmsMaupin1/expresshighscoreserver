const express = require('express');
const jsonBody = require('body/json');
const app = express();
const port = 3000;

const scores = [
    {
        name: "Edwin",
        score: 50
    },
    {
        name: "David",
        score: 39
    }
];

function addAndSortScores(score) {
    scores.push(score);
    scores.sort( (a, b) => b.score - a.score);
    scores.splice(3, scores.length - 1);
}

app.get('/scores', (req, res) => {
    res.statusCode = 200;
    res.setHeader('content-type', 'application/javascript');
    res.end(JSON.stringify(scores));
});

app.post('/scores', (req, res) => {
    jsonBody(req, res, (err, body) => {
        res.statusCode = 201;
        addAndSortScores(body);
        res.end(JSON.stringify(scores));
    })
});

app.get('*', (req, res) => {
    res.statusCode = 404;
    res.end("404 not found bruh");
});

app.post('*', (req, res) => {
    res.statusCode = 404;
    res.end('404 not found bruh');
});

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});